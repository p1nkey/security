import "console"

rule TestCaseRule_OpenHarmony_SA_2024_0118
{
    meta:
        date = "2024-1-18"
        file = "/system/lib/file.so"

    strings:
        $vul = {00 f0 ?? ef 01 46 20 46 00 f0 ?? ef}
        $fix = {00 f0 ?? ef ?? 4d 01 46 00 28 20 46 7d 44 08 bf 29 46}

    condition:
        ((not $vul) or $fix) and console.log("OpenHarmony-SA-2023-0301 testcase pass")
}